package com.cejv659.jaxbtojson;

import com.cejv659.jaxbtojson.bean.FishData;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.eclipse.persistence.jaxb.MarshallerProperties;

/**
 * Based on
 * http://examples.javacodegeeks.com/core-java/xml/bind/jaxb-json-example/
 *
 * Example of Marshalling where an object is converted to XML or JSON notation
 *
 * For this to work you must use an implementation of JAXB called MOXy. There is
 * a dependency in the Maven pom.xml for this. You must also have a special
 * properties file titled "jaxb.properties" that is in the same package as the
 * bean classes.
 *
 * In this NetBeans project it has been placed in the matching package in the
 * resources folder. If you place this files in the same folder as the source
 * code it will not be included in the final jar file.
 */
public class MarshallerDemo {

    public void perform() throws JAXBException {
        // Creating a new employee pojo object with data

        FishData fishData = new FishData(6, "African Brown Knife",
                "Xenomystus nigri", "6.0-8.0", "5-19 dH", "72-78F", "12 in TL",
                "Africa", "", "", "Carnivore");

        // Create a JaxBContext
        JAXBContext jc = JAXBContext.newInstance(FishData.class);

        // Create the Marshaller Object using the JaxB Context
        Marshaller marshaller = jc.createMarshaller();

        // Set the Marshaller media type to JSON or XML
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE,
                "application/json");
        //"application/xml");

        // Set it to true if you need to include the JSON root element in the JSON output
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);

        // Set it to true if you need the JSON output to formatted
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        // Marshal the employee object to JSON and print the output to console
        marshaller.marshal(fishData, System.out);
    }

    /**
     * @param args
     * @throws JAXBException Marshaller POJO to JSON using EclipseLink MOXy
     */
    public static void main(String[] args) throws JAXBException {
        MarshallerDemo md = new MarshallerDemo();
        md.perform();
        System.exit(0);

    }

}
