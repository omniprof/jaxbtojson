package com.cejv659.jaxbtojson;

import com.cejv659.jaxbtojson.bean.FishData;
import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.eclipse.persistence.jaxb.UnmarshallerProperties;

/**
 * Based on
 * http://examples.javacodegeeks.com/core-java/xml/bind/jaxb-json-example/
 *
 * Example of Unmarshalling where a string in either JSON or XML notation is
 * converted to an object
 *
 * For this to work you must use an implementation of JAXB called MOXy. There is
 * a dependency in the Maven pom.xml for this. You must also have a special
 * properties file titled "jaxb.properties" that is in the same package as the
 * bean classes.
 *
 * In this NetBeans project it has been placed in the matching package in the
 * resources folder. If you place this files in the same folder as the source
 * code it will not be included in the final jar file.
 */
public class UnmarshallerDemo {

    private final String jsonStr = "{\"Fish\":{\"commonName\":\"1-African Brown Knife\",\"diet\":\"Carnivore\",\"fishSize\":\"12 in TL\",\"id\":6,\"kh\":\"5-19 dH\",\"latin\":\"Xenomystus nigri\",\"ph\":\"6.0-8.0\",\"speciesOrigin\":\"Africa\",\"stocking\":\"\",\"tankSize\":\"\",\"temp\":\"72-78F\"}}";
    private final String xmlStr = "<Fish><commonName>2-African Brown Knife</commonName><diet>Carnivore</diet><fishSize>12 in TL</fishSize><id>6</id><kh>5-19 dH</kh><latin>Xenomystus nigri</latin><ph>6.0-8.0</ph><speciesOrigin>Africa</speciesOrigin><stocking></stocking><tankSize></tankSize><temp>72-78F</temp></Fish>";

    /**
     * Demonstration of unmarshalling
     *
     * @throws JAXBException
     */
    public void perform() throws JAXBException {
        // Create a JaxBContext
        JAXBContext jc = JAXBContext.newInstance(FishData.class);

        // Create the Unmarshaller Object using the JaxB Context
        Unmarshaller unmarshaller = jc.createUnmarshaller();

        // Set the Unmarshaller media type to JSON or XML
        unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE,
                "application/json");
                //"application/xml");

        // Set it to true if you need to include the JSON root element in the
        // JSON input
        unmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);

        // Create the StreamSource by creating StringReader using the JSON input
        StreamSource json = new StreamSource(
                new StringReader(jsonStr));
                //new StringReader(xmlStr));

        // Getting the employee pojo again from the json
        FishData fish = unmarshaller.unmarshal(json, FishData.class).getValue();

        // Print the employee data to console
        System.out.println("Fish:\n" + fish);

    }

    /**
     * Where it begins
     *
     * @param args
     * @throws JAXBException Unmarshaller JSON to POJO using EclipseLink MOXy
     */
    public static void main(String[] args) throws JAXBException {
        UnmarshallerDemo umd = new UnmarshallerDemo();
        umd.perform();
        System.exit(0);
    }

}
